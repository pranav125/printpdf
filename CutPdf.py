import PyPDF2 as pdf
pdffile = open('input.pdf','rb')
read = pdf.PdfFileReader(pdffile)
toWrite = pdf.PdfFileWriter()
# startpage = 0
# endpage = 10
# for page in range(startpage, endpage):
#     toWrite.addPage(read.getPage(page))

resultPDF = open("solution.pdf",'wb')
toWrite.addPage(read.getPage(read.getNumPages() - 1))
toWrite.write(resultPDF)
pdffile.close()
resultPDF.close()